/*
	Environmentalist - src/environmentalist.ts
	Written by Smartfridge Solutions - https://smartfridge.solutions

	File Contributors:
	 * Quinn Lane (https://quinnlane.dev/) <hello@quinnlane.dev>

	--- Legal Mumbo Jumbo ---

	This file is part of Environmentalist, which is in itself part of Pluto.

	Environmentalist is free software: you can redistribute it and/or modify it under the terms of
	the GNU Lesser General Public License as published by the Free Software Foundation,
	either version 3 of the License, or (at your option) any later version.

	Environmentalist is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License along with Environmentalist.
	If not, see <https://www.gnu.org/licenses/>.
 */

import { config } from 'dotenv'

export default function () {
	config()
}